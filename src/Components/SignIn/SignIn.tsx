import { signIn } from "../../Services/index";
import type { SignInPropsTypes } from "../../Interfaces/Authentication";
import { useForm } from "react-hook-form";
import { useState } from "react";

export const SignIn = ({
  onChange,
  formState,
  setFormState,
  setUser,
}: SignInPropsTypes) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [authError, setAuthError] = useState("");

  function onSubmit() {
    signIn(formState).then((signInResult) => {
      setUser(signInResult);
      if (signInResult.challengeName === "NEW_PASSWORD_REQUIRED") {
        setFormState({ ...formState, formType: "newPassword" });
      } else if (
        signInResult instanceof Error &&
        signInResult.name === "NotAuthorizedException"
      ) {
        setAuthError("Invalid credentials");
      }
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="d-flex flex-column">
      <label className="mt-2 mb-3">
        Username:
        <input
          {...register("username", {
            onChange: (e) => onChange(e),
            required: true,
          })}
          className="form-control bg-transparent border border-secondary mt-2 text-white"
        />
      </label>
      {errors.username && (
        <span className="text-danger">This field is required</span>
      )}
      <label className="mt-2 mb-3">
        Password:
        <input
          type="password"
          {...register("password", {
            onChange: (e) => onChange(e),
            required: true,
          })}
          className="form-control bg-transparent border border-secondary mt-2 text-white"
        />
      </label>
      {errors.password && (
        <span className="text-danger">This field is required</span>
      )}
      {<span className="text-danger">{authError}</span>}
      <button type="submit" className="btn btn-primary mt-5 mb-3">
        Sign In
      </button>
    </form>
  );
};
export default SignIn;
