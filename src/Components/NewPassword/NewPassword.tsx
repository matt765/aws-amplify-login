import type { NewPasswordPropsTypes } from "../../Interfaces/Authentication";
import { setNewPassword } from "../../Services/index";
import { useForm } from "react-hook-form";

const NewPassword = ({ onChange, formState, user }: NewPasswordPropsTypes) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  function onSubmit() {
    setNewPassword(formState, user);
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)} className="d-flex flex-column">
        <label className="mt-2 mb-3">
          Enter new password
          <input
            {...register("password", { required: true })}
            type="password"
            onChange={onChange}
            className="form-control bg-transparent border border-secondary mt-2 text-white"
          />
        </label>
        {errors.password && <span className="text-danger" >This field is required</span>}
        <button type="submit" className="btn btn-primary mt-5 mb-3">
          Set new password
        </button>
      </form>
    </div>
  );
};
export default NewPassword;
