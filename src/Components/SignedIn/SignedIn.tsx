import { useEffect } from "react";

interface Props {
  currentToken: string;
}

const SignedIn = ({ currentToken }: Props) => {
  useEffect(() => {
    const address =
      "https://m8ltbejfo0.execute-api.eu-west-1.amazonaws.com/teams";
    if (currentToken.length > 0) {
      fetch(address, {
        method: "GET",
        headers: {
          Authorization: currentToken,
        },
      })
        .then((response) => response.json())
        .then((data) => console.log(data))
        .finally(() => window.location.replace("http://google.com"));
    }
  }, [currentToken]);
  return <></>;
};
export default SignedIn;
