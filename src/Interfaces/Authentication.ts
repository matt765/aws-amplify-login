export interface AuthTypes {
  username: string;
  password: string;
  newPassword: string;
  authCode: string;
  formType: string;
}

export interface NewPasswordPropsTypes {
  onChange: (event: React.FormEvent<HTMLInputElement>) => void;
  formState: AuthTypes;
  user: unknown;
}

export interface SignInPropsTypes {
  onChange: (event: React.FormEvent<HTMLInputElement>) => void;
  formState: AuthTypes;
  setFormState: (newFormState: AuthTypes) => void;
  setUser: (user: unknown) => void;
}
