import { useState, useEffect } from "react";
import "./Styles/main.css";
import { checkUser, setAuthListener, getToken } from "./Services/index";
import SignIn from "./Components/SignIn/SignIn";
import SignedIn from "./Components/SignedIn/SignedIn";
import NewPassword from "./Components/NewPassword/NewPassword";
import LoginForm from "./Components/LoginForm/LoginForm";

const initialFormState = {
  username: "",
  password: "",
  newPassword: "",
  authCode: "",
  formType: "signIn",
};

function App() {
  const [formState, setFormState] = useState(initialFormState);
  const [user, setUser] = useState<unknown>(null);
  const [currentToken, setCurrentToken] = useState("");

  useEffect(() => {
    checkUser(formState, setFormState);
    setAuthListener(formState, setFormState);
  }, []);

  useEffect(() => {
    if (formType === "signedIn") {
      getToken(setCurrentToken);
    }
  }, [formState]);

  function onChange(e) {
    setFormState({ ...formState, [e.target.name]: e.target.value });
  }

  const { formType } = formState;

  return (
    <div className="App">
      {formType === "signIn" && (
        <LoginForm formType={formType}>
          <SignIn
            onChange={onChange}
            formState={formState}
            setFormState={setFormState}
            setUser={setUser}
          />
        </LoginForm>
      )}
      {formType === "newPassword" && (
        <LoginForm formType={formType}>
          <NewPassword onChange={onChange} formState={formState} user={user} />
        </LoginForm>
      )}
      {formType === "signedIn" && <SignedIn currentToken={currentToken} />}
    </div>
  );
}

export default App;
