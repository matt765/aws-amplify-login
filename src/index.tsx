import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Amplify from "aws-amplify";
import 'bootstrap/dist/css/bootstrap.css';

Amplify.configure({
  Auth: {
    region: "eu-west-1",
    userPoolId: "eu-west-1_FD30XMCXV",
    userPoolWebClientId: "4p1jd0p2d73ni5fode827h2cq5",
  },
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

