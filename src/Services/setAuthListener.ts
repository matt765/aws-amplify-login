import { Hub } from "aws-amplify";
import type { AuthTypes } from "../Interfaces/Authentication";

export async function setAuthListener(
  formState: AuthTypes,
  setFormState: (newFormState: AuthTypes) => void
) {
  Hub.listen("auth", (data) => {
    switch (data.payload.event) {
      case "signIn":
        setFormState({ ...formState, formType: "signedIn" });
        break;
      case "signOut":
        setFormState({ ...formState, formType: "signIn" });
        break;
      case "signIn_failure":
        console.log("user sign in failed");
        break;
    }
  });
}
