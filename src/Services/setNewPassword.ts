import type { AuthTypes } from "../Interfaces/Authentication";
import { Auth } from "aws-amplify";

export async function setNewPassword(formState: AuthTypes, user: any) {
  const { newPassword } = formState;
  try {
    Auth.completeNewPassword(user, newPassword)
      .then((user) => {
        console.log(user);
      })
      .catch((e) => {
        console.log(e);
      });
  } catch (err) {
    console.log(err);
  }
}
