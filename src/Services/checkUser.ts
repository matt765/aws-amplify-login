import type { AuthTypes } from "../Interfaces/Authentication";
import { Auth } from "aws-amplify";

export async function checkUser(formState: AuthTypes, setFormState: (newFormState: AuthTypes) => void) {
  try {
    await Auth.currentAuthenticatedUser();
    setFormState({ ...formState, formType: "signedIn" });
  } catch (err) {
    console.log(err);
  }
}
