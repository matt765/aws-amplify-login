import type { AuthTypes } from "../Interfaces/Authentication";
import { Auth } from "aws-amplify";

export async function signIn(formState: AuthTypes) {
  const { username, password } = formState;

  try {
    const user = await Auth.signIn(username, password);
    return user;
  } catch (err) {
    return err;
  }
}
