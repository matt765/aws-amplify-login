import { Auth } from "aws-amplify";

export async function getToken(setCurrentToken: (token: string) => void) {
  try {
    await Auth.currentSession().then((data) => {
      for (const [key, value] of Object.entries(data)) {
        if (key === "accessToken") {
          setCurrentToken(value.jwtToken);
        }
      }
    });
  } catch (err) {
    console.error(err);
  }
}